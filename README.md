# EBRAINS Software Distribution

<img align="right" src="ebrains_logo_ESD.png" width="33%" alt="EBRAINS Software Distribution Logo"/>

The **EBRAINS Software Distribution (ESD)** provides a curated, versioned, jointly developed and deployed software environment on the web-based interactive computing environment ("*Jupyter notebooks*"), and across EBRAINS HPC sites.
To take full advantage of HPC performance capabilities, the deployed software environments are carefully optimised for site-specific features such as high-performance data transfer or CPU and accelerator architectures.
Furthermore, the distribution maximises reproducibility aspects in terms of software dependencies, as modern user software stacks exhibit significant complexity.
In addition, containerisation technology allows users to easily use the same software environment not only on the federated infrastructure, but also locally, maximising the ease of switching between development and, e.g., HPC production use.
In addition, compatibility with the EuroHPC ecosystem will also open up a wide range of additional systems.
Users of workflow engines also benefit as the same software packages can be used everywhere in multi-site environments, facilitating migration within the federated infrastructure.
